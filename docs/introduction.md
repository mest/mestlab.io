# Introduction

MEST represents a library that can be used to create and execute Simulations. Every Simulation contains a set of Components that are connected with each other via Ports. The actual logic that is executed will be described by the Component, too.

MEST also includes a user interface that can be used to create Simulations with existing Components.

## Component

A Component represents an entity inside a Simulation that will be executed. The Component will descibe Ports to publish their current state (output values) or to be able to be connected to output Ports of other Components in order to read their output values.

In addition to the Ports, a Component should provide it's logic (representation of what this Component should simulate). There can up to three types of logic methods provided by the Component:
- *Tick Logic* (will be called periodically corresponding to the Components periodicity)
- *Output Request Logic* (will be called, when the output value of an [ContinuousOutputPort](#ContinuousOutputPort) will be read)
- *Event Handler Logic* (will be called for each received event of an [EventInputPort](#EventInputPort))

More details about these logic methods can be found in the section [Runtime](#Runtime).

## Ports

Ports will be used to transfer data between Components. There are two principles used for this:
- *Pull* (Input Port reading the current output value of an Output Port)
  - Some Components logic will be called periodically (i.e. an engine control unit running at 10 MHz or a clock generator at 1 KHz). After the execution the state of the Component may be published. 
- *Push* (Output Port publishing an event that will be received and consumed by a Input Port)
  - When using Ports of the *Pull* principle, Components have to actively request the data. If they want to receive every value, they have to ask for the data at the same periodicity of that Component. To overcome this problem, the *Push* principle can be used. With this principle, events will be published. It is guaranteed, that every event will be published to the connected Component.

MEST currently includes the following types of Ports:
- *Pull* principle
  - [DiscreteOutputPort](#DiscreteOutputPort)
  - [ContinuousOutputPort](#ContinuousOutputPort)
  - [InputPort](#InputPort)
- *Push* principle
  - [EventOutputPort](#EventOutputPort)
  - [EventInputPort](#EventInputPort)

### DiscreteOutputPort

(*Output Port* using *Pull* principle)

The DiscreteOutputPort can be used to publish an output value that can then be read by connected Components (see [InputPort](#InputPort)). 

This port type is the standard Port to be used to publish output values for other Components (see [ContinuousOutputPort](#ContinuousOutputPort)). In general, these Ports should represent the public state of an Component that is executed periodically.

### ContinuousOutputPort

(*Output Port* using *Pull* principle)

Sometimes a given output value is only relevant, when beeing requested or read by another Component (i.e. the output of an sensor). This is espacially true, if the corresponding Component can use mathematic equations to calculate the output value for a Port. In this case, it can make sense to use a ContinuousOutputPort instead of a [DiscreteOutputPort](#DiscreteOutputPort).

A Continuous OutputPort is nearly identical to an [DiscreteOutputPort](#DiscreteOutputPort). There are two differences mentioned below:
- When the output value will be requested, the *Output Request Logic* of the Component will be called.
- TODO

### InputPort

(*Input Port* using *Pull* principle)

Components may need information from other Components. This can be done with an InputPort. This Port can be used to read output values of [DiscreteOutputPort](#DiscreteOutputPort) and/or [ContinuousOutputPort](#ContinuousOutputPort).

### EventOutputPort

(*Output Port* using *Push* principle)

These Ports can be used to publish events. These events will be published to all connected [EventInputPorts](#EventInputPort).

### EventInputPort

(*Input Port* using *Push* principle)

These Ports define the *Event Handler Logic* that will be called for every received event from a [EventOutputPort](#EventOutputPort).

## Runtime

### General Runtime Concepts

The main task of the Runtime is to execute Component logics in a so called *step* and increase the current time afterwards. The Component logics to be executed depends on the Component Configuration (i.e. the periodicity or run on output value request) or other factors like an received event. How and when a Component logic will be executed is described in [Component Logic Scheduling](#Component-Logic-Scheduling).

!> The most important rule for the Runtime is, that **the state of Components will not change during a step**. If a Component does change an output value of it's output Port, this value can only be read after the Simulation *step* has been completed. This means, that the execution order of Components will never impact the Simulation result. This rule applies to events, too (*push* principle). They can only be received and consumed after the *step* has been finished.

### Component Logic Scheduling

For every *step*, the Scheduler will calculate which Component Logics have to be called. As mentioned in [Component](#Component), there are three types of Component Logics that can be called. 

?> The *Tick Logic* and *Output Request Logic* for a single Component will only be called once in a step. The *Event Handler Logic* may be called multiple times depending on the number of received events.

The order in which the Logics will be called in a *step* is as follows:
1. Gather all *Output Request Logics* with a matching execution condition
  1. Run all Components *Output Request Logic* (if the conditions for the logic match)
  1. Update the state of the Simulation for the executed Components (commit the output values of the [ContinuousOutputPorts](#ContinuousOutputPort) so they can be read by other Components in this step)
1. Execute all *Event Handler Logics* with a matching execution condition
1. Execute all *Tick Logics* with a matching execution condition
1. Commit the output values for [DiscreteOutputPorts](#DiscreteOutputPort) and publish the events of the [EventOutputPorts](#EventOutputPort).


!> Keep in mind, that the output value of [ContinuousOutputPorts](#ContinuousOutputPort) will be updated at the start of the *step*. In contrast to the main rule for the Runtime, the output value of [ContinuousOutputPorts](#ContinuousOutputPort) can be read by other Components in the step.

If a Component logic has to be called depends on various conditions. The conditions are mentioned below.

#### Tick Logic Execution conditions

The *Tick Logic* will only be called if the `current time == last tick logic execution of this Component + Component periodicity`

?> Example

#### Output Request Logic Execution conditions

In general the *Output Request Logic* can only be called if the Component contains at least one [ContinuousOutputPort](#ContinuousOutputPort).

Apart from that, there are two configurations for the continuous behaviour described below.

##### Configuration: RunAlways

If this configuration is set, the logic will be executed in every step.

> [!NOTE]
> It is irrelevant wether the port is connected or not.

##### Configuration: RunOnRequest

If this configuration is set the logic will only be executed, when the output value may be read by other Components in this step. This is the case, when all of the following conditions are true:
- At least one Component is connected to this Components [ContinuousOutputPort](#ContinuousOutputPort).
- At least one of the connected Components will be triggered in this step (either *Tick Logic*, *Output Request Logic*, or *Event Handler Logic*)

!> **Special Case 1:** A Component has an InputPort and a ContinuousOutputPort and these two are connected. If the Component configuration for the continuous behaviour is set to *RunOnRequest*, the *Output Request Logic* will be called for every step.

!> **Special Case 2:** Two Components have an InputPort and a ContinuousOutputPort. The ContinuousOutputPort of Component 1 is connected to the InputPort of Component 2. Both Components have the configuration for the continuous behaviour set to *RunOnRequest*. In this case, both Components *Output Request Logic* will be called. **Warning:** Keep in mind, that the output values of the ContinuousOutputPort from Component 1 will be the value set in the last step when read by Component 2.

#### Event Handler Logic Execution conditions

The *Event Handler Logic* for a given [EventInputPort](#EventInputPort) will be triggered, if the following conditions are true:
* events were published to this port in a previous *step*.
* `current time == last tick logic execution of this Component + Component periodicity`

!> *Event Handler Logics* will only be called at the periodicity configured for the Component. This means, that the *Event Handler Logics* will be called only at the times the *Tick Logic* will be called, too.

!> Keep in mind, that the *Event Handler Logic* will not be called directly after an event has been published by an [EventOutputPort](#EventOutputPort). The *Event Handler Logic* will be called in the next possible *step* matching the conditions above.