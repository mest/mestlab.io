# MEST

> **M**odular **E**ntity **S**imulation **T**oolkit

**MEST** is a simulation toolkit to simulate components communicating via ports written in and for [Kotlin](http://kotlinlang.org/).    
Use the *framework library* to start a source code based project or use the *user interface* to design simulations and entities.

[GitLab](https://gitlab.com/mest/mest)
[Get Started](README.md)

![color](#e5ffe8)
